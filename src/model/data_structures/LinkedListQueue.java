/**
 * En base a Metodos obtenidos de:https://codereview.stackexchange.com/questions/139975/generic-queue-array-and-linked-list-implementation
 */

package model.data_structures;

import java.util.NoSuchElementException;

public class LinkedListQueue <E> implements IQueue<E> {
	private Node<E> first, last;
	private int size=0;

	private static class Node<E> {
		private E data;

		private Node<E> back;

		public Node(E element) {
			data = element;
		}
	}


	public void enqueue(E element) {
		Node<E> newElement = new Node<E>(element);
		if (first == null) {
			first = newElement;
			size++;
		} else {
			if (first.back == null) {
				first.back = newElement;
				size++;
			} else {
				last.back = newElement;
				size++;
			}

			last = newElement;
		}

	}


	public E element() throws NoSuchElementException {
		if (first == null) {
			throw new NoSuchElementException("Queue does not contain any items.");
		}

		return first.data;
	}


	public E dequeue() throws NoSuchElementException {
		if (first == null) {
			throw new NoSuchElementException("Queue does not contain any items.");
		}

		E output = first.data;
		first = first.back;
		return output;
	}



	@Override
	public E peek() {
		return first == null ? null : first.data;
	}




	@Override
	public boolean isEmpty() {
		if (first == null)
		{
			return true;
		}
		return false;
	}
	public int size()
	{
		return size;
	}
}
