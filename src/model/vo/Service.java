package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	private String tripId;
	private String taxiId;
	private String tripSeconds;
	private String tripMiles;
	private String tripTotal;
	private String tripStartTime;


	
	public Service(String tripId, String taxiId, String tripSeconds, String tripMiles, String tripTotal, String tripStartTime)
	{
		this.tripId=tripId;
		this.taxiId=taxiId;
		this.tripSeconds=tripSeconds;
		this.tripMiles=tripMiles;
		this.tripTotal=tripTotal;
		this.tripStartTime=tripStartTime;
	}
	

	public String getTripId() {
		return tripId;
	}





	public void setTripId(String tripId) {
		this.tripId = tripId;
	}





	public String getTaxiId() {
		return taxiId;
	}





	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}





	public String getTripSeconds() {
		return tripSeconds;
	}





	public void setTripSeconds(String tripSeconds) {
		this.tripSeconds = tripSeconds;
	}





	public String getTripMiles() {
		return tripMiles;
	}





	public void setTripMiles(String tripMiles) {
		this.tripMiles = tripMiles;
	}





	public String getTripTotal() {
		return tripTotal;
	}





	public void setTripTotal(String tripTotal) {
		this.tripTotal = tripTotal;
	}





	public String getTripStartTime() {
		return tripStartTime;
	}





	public void setTripStartTime(String tripStartTime) {
		this.tripStartTime = tripStartTime;
	}

	
	
	
	
	@Override
	public int compareTo(Service o) {
		return tripStartTime.compareTo(o.getTripStartTime());
	}
}
