package model.logic;

import java.io.FileReader;
import java.util.Comparator;

import com.google.gson.Gson;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedListQueue;
import model.data_structures.LinkedListStack;
import model.data_structures.ListaDoblementeEncadenada;
import model.vo.Taxi;

import model.vo.Service;
import model.vo.ServiceVo;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private LinkedListQueue<Service> colaServicios;
	private LinkedListStack<Service> pilaServicios;
	public ListaDoblementeEncadenada<Service> listaServicios;
	
	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		
		colaServicios=new LinkedListQueue<Service>();
		pilaServicios=new LinkedListStack<Service>();
		listaServicios=new ListaDoblementeEncadenada<Service>();
		
		Gson gson = new Gson();
		int contador=0;
		try
		{
			ServiceVo[] myTypes = gson.fromJson(new FileReader(serviceFile), ServiceVo[].class);
			
			for (int i = 0;i < myTypes.length; i++) 
			{
				ServiceVo nServ=myTypes[i];
				String taxiIdentification= nServ.getTaxi_id()==null?"":nServ.getTaxi_id();
				if(taxiIdentification.equalsIgnoreCase(taxiId))
				{
					Service servicio=new Service(nServ.getTrip_id()==null?"":nServ.getTrip_id(),taxiIdentification,
							nServ.getTrip_seconds()==null?"":nServ.getTrip_seconds(),nServ.getTrip_miles()==null?"":nServ.getTrip_miles(),
							nServ.getTrip_total()==null? "":nServ.getTrip_total(),nServ.getTrip_start_timestamp()==null? "":nServ.getTrip_start_timestamp());
						
					colaServicios.enqueue(servicio);
					pilaServicios.push(servicio);
					listaServicios.addInOrderC(servicio);
					contador++;
				}
							
	
				
			}
//			for(int i=0;i<listaServicios.size();i++)
//			{
//				System.out.println("Servicio: "+listaServicios.dar(i).getTripStartTime());
//
//			}
//			
//			System.out.println("se guardaron :"+listaServicios.size()+"\n");
//			System.out.println("Se guardaron en la cola: ");
//			for(int i=0;i<listaServicios.size();i++)
//			{
//				System.out.println("Servicio: "+colaServicios.dequeue().getTripStartTime());
//			}
//			
//			System.out.println("\n Se guardaron en la Pila: ");
//			for(int i=0;i<listaServicios.size();i++)
//			{
//				System.out.println("Servicio: "+pilaServicios.pop().getTripStartTime());
//			}
			
	
		
		}
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}
	
		
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		int [] resultado = new int[2];
		resultado[0]=1;
		resultado[1]=0;
		Service []ordenados=new Service[pilaServicios.size()];
		for (int i = 0; i < ordenados.length; i++) {
			ordenados[i]=pilaServicios.pop();
		}
		int actual=0;
		int comparar=1;
		while(comparar<ordenados.length-1)
		{
			if(ordenados[actual].compareTo(ordenados[comparar])>=0)
			{
				resultado[0]=resultado[0]+1;
				actual=comparar;
				comparar++;
			}
			else 
			{
				resultado[1]=resultado[1]+1;
				comparar++;
				
			}
		}
				
		return resultado;
	}

	@Override
	public int [] servicesInOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		int [] resultado = new int[2];
		resultado[0]=1;
		resultado[1]=0;
		Service []ordenados=new Service[colaServicios.size()];
		for (int i = 0; i < ordenados.length; i++) {
			ordenados[i]=colaServicios.dequeue();
		}
		int actual=0;
		int comparar=1;
		while(comparar<ordenados.length-1)
		{
			if(ordenados[actual].compareTo(ordenados[comparar])<=0)
			{
				resultado[0]=resultado[0]+1;
				actual=comparar;
				comparar++;
			}
			else 
			{
				resultado[1]=resultado[1]+1;
				comparar++;
				
			}
		}
				
				
		return resultado;
	}


}
